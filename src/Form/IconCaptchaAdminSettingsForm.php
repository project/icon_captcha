<?php

namespace Drupal\icon_captcha\Form;

use Drupal\Core\Asset\LibrariesDirectoryFileFinder;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Icon Captcha settings for this site.
 */
class IconCaptchaAdminSettingsForm extends ConfigFormBase {

  /**
   * The library file finder object.
   *
   * @var \Drupal\Core\Asset\LibrariesDirectoryFileFinder
   */
  protected $libraryFileFinder;

  /**
   * {@inheritdoc}
   */
  public function __construct(LibrariesDirectoryFileFinder $libraryFileFinder) {
    $this->libraryFileFinder = $libraryFileFinder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('library.libraries_directory_file_finder'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'icon_captcha_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['icon_captcha.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('icon_captcha.settings');

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
    ];

    $form['general']['theme'] = [
      '#type' => 'select',
      '#default_value' => $config->get('theme'),
      '#options' => [
        'light' => $this->t('Light'),
        'dark' => $this->t('Dark'),
      ],
      '#description' => $this->t('Set the theme of the icon captcha validation'),
      '#title' => $this->t('Theme'),
    ];

    if (!$this->libraryFileFinder->find('IconCaptcha-PHP')) {
      $this->messenger()->addWarning(icon_captcha_get_library_missing_message());
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('icon_captcha.settings');
    $config
      ->set('theme', $form_state->getValue('theme'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
