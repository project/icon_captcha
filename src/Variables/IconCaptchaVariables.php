<?php

namespace Drupal\icon_captcha\Variables;

/**
 * Class for defining icon_captcha constants.
 */
class IconCaptchaVariables {

  public const ICON_CAPTCHA_LIBRARY_DIR = 'IconCaptcha-PHP';

  /**
   * Gets the absolute library path.
   */
  public static function getAbsoluteLibraryPath() {
    $libraryPath = \Drupal::service('library.libraries_directory_file_finder')->find(self::ICON_CAPTCHA_LIBRARY_DIR);
    return \Drupal::service('file_system')->realpath($libraryPath);
  }

}
