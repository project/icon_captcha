<?php

namespace Drupal\icon_captcha\Controller;

use Drupal\Core\Cache\CacheableResponse;
use Drupal\icon_captcha\Variables\IconCaptchaVariables;

require_once IconCaptchaVariables::getAbsoluteLibraryPath() . '/src/captcha-session.class.php';
require_once IconCaptchaVariables::getAbsoluteLibraryPath() . '/src/captcha.class.php';

use IconCaptcha\IconCaptcha;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Handles the icon_captcha response data.
 *
 * @todo This is a rewrite of "IconCaptcha-PHP/src/captcha-request.php" if there
 * is a proper API in the future, we can adjust this controller accordingly.
 */
class CaptchaRequest extends ControllerBase {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The controller constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {
    // Set the IconCaptcha options:
    IconCaptcha::options(icon_captcha_get_options());

    $response = new CacheableResponse();
    $getPayload = $this->requestStack->getCurrentRequest()->query->get('payload');
    $postPayload = $this->requestStack->getCurrentRequest()->request->get('payload');
    // HTTP GET:
    if (!empty($getPayload) && !$this->isAjaxRequest()) {
      $decodedGetPayload = $this->decodePayload($getPayload);
      // Validate the payload content:
      if (empty($decodedGetPayload) || empty($decodedGetPayload['i']) || !is_numeric($decodedGetPayload['i'])) {
        throw new \Exception("Bad Request", 1);
      }
      // @todo The following code is very dirty and shouldn't be shipped like
      // this. We basically tried to comment out the "session" parts and other
      // non drupal conform code inside the external "IconCaptcha::getImage()"
      // function, to see if we can make it work somehow. The Problem is, that
      // the library works alot with the session object, which is not Drupal
      // conform, because we do not want to work with another session, but
      // instead work with the Drupal session. This is unfortunatly not possible
      // ,so we have to postpone this project for the time being...

      // ob_start();
      // IconCaptcha::getImage($decodedGetPayload['i']);
      // $str = ob_get_clean();
      // $response->setContent($str);
      // $response->setStatusCode(Response::HTTP_OK);
      // // Set the content type header to the PNG MIME-type.
      // $response->headers->set('Content-Type', 'image/png');
      // // Disable caching of the image.
      // $response->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate, post-check=0, pre-check=0');
      // $response->headers->set('Pragma', 'no-cache');
      // $response->headers->set('Expires', 0);
    }
    elseif (!empty($postPayload) && $this->isAjaxRequest()) {
      $decodedPostPayload = $this->decodePayload($postPayload);
      // Validate the payload content.
      if (empty($decodedPostPayload) || empty($decodedPostPayload['a']) || empty($decodedPostPayload['i']) || !is_numeric($decodedPostPayload['a']) || !is_numeric($decodedPostPayload['i'])) {
        throw new \Exception("Bad Request", 1);
      }

      switch ((int) $decodedPostPayload['a']) {
        // Requesting the image hashes.
        case 1:
          // Validate the theme name. Fallback to light.
          $theme = (!empty($decodedPostPayload['t']) && is_string($decodedPostPayload['t'])) ? $decodedPostPayload['t'] : 'light';
          // Echo the captcha data.
          $response->setStatusCode(Response::HTTP_OK);
          $response->setContent(IconCaptcha::getCaptchaData($theme, $decodedPostPayload['i']));
          $response->headers->set('Content-Type', 'text/plain');
          break;

        case 2:
          // Setting the user's choice.
          if (IconCaptcha::setSelectedAnswer($decodedPostPayload)) {
            $response->setStatusCode(Response::HTTP_OK);
          }
          break;

        // Captcha interaction time expired.
        case 3:
          // @todo Might lead to log out:
          $session = $this->requestStack->getCurrentRequest()->getSession();
          $session->invalidate();
          $response->setStatusCode(Response::HTTP_OK);
          break;

        default:
          break;
      }
    }
    return $response;
  }

  /**
   * Whether or not the Request is an Ajax Request.
   *
   * @todo Is this even needed, when working with Drupal??
   *
   * @return bool
   *   Whether or not it is a ajax request.
   */
  public function isAjaxRequest() {
    $server = $this->requestStack->getCurrentRequest()->server;
    return (!empty($server->get('HTTP_X_REQUESTED_WITH')) && strtolower($server->get('HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest');
  }

  /**
   * Function to decode the payload.
   */
  public function decodePayload($payload) {
    // Base64 decode the payload.
    $payload = base64_decode($payload);
    if ($payload === FALSE) {
      throw new \Exception("Bad Request", 1);
    }

    // JSON decode the payload.
    return json_decode($payload, TRUE);
  }

}
