(function (Drupal, drupalSettings, IconCaptcha) {
    Drupal.behaviors.initIconCaptcha = {
        attach: function (context, settings) {
            console.log(drupalSettings.path.baseUrl)
            IconCaptcha.init('.iconcaptcha-holder', {
                general: {
                    validationPath: drupalSettings.path.baseUrl + 'ajax/icon-captcha/captcha-request', // required, change path according to your installation.
                    fontFamily: 'Poppins',
                    credits: 'show',
                },
                security: {
                    clickDelay: 500,
                    hoverDetection: true,
                    enableInitialMessage: true,
                    initializeDelay: 500,
                    selectionResetDelay: 3000,
                    loadingAnimationDelay: 1000,
                    invalidateTime: 1000 * 60 * 2, // 2 minutes, in milliseconds
                },
                messages: {
                    initialization: {
                        verify: 'Verify that you are human.',
                        loading: 'Loading challenge...'
                    },
                    header: 'Select the image displayed the <u>least</u> amount of times',
                    correct: 'Verification complete.',
                    incorrect: {
                        title: 'Uh oh.',
                        subtitle: "You've selected the wrong image."
                    },
                    timeout: {
                        title: 'Please wait 60 sec.',
                        subtitle: 'You made too many incorrect selections.'
                    }
                }
            });
        }
    }
})(Drupal, drupalSettings, IconCaptcha);
